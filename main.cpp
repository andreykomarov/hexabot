#include <xcb/xcb.h>
#include <xcb/xproto.h>
#include <boost/log/trivial.hpp>

#include "xcb/hexagon.h"
#include "xcb/screenshot.h"
#include "xcb/analyzer.h"

#include <chrono>

#include <xcb/xtest.h>

#define LEFT_KEYCODE 113
#define RIGHT_KEYCODE 114
#define SPACE_KEYCODE 65

int main() {
    sleep(3);
    int screen_num;
    xcb_connection_t *connection = xcb_connect(NULL, &screen_num);
    BOOST_ASSERT(screen_num == 0);
    auto window = get_hexagon_window(connection);
    if (!window)
    {
        BOOST_LOG_TRIVIAL(error) << "\"Super Hexagon\" window not found";
        return 1;
    }

    auto size = get_window_size(connection, *window);
    BOOST_LOG_TRIVIAL(info) << "Window size: " << size.first << "x" << size.second;
//    auto screen = take_screenshot(connection, *window);
//    BOOST_LOG_TRIVIAL(info) << "Got " << screen.size() << " bytes";
    const xcb_setup_t *setup = xcb_get_setup(connection);
    xcb_screen_t *screen = xcb_setup_roots_iterator(setup).data;

    xcb_window_t my_window = xcb_generate_id(connection);
    xcb_create_window (connection,                    /* Connection          */
                       XCB_COPY_FROM_PARENT,          /* depth (same as root)*/
                       my_window,                     /* window Id           */
                       screen->root,                  /* parent window       */
                       0, 0,                          /* x, y                */
                       size.first, size.second,       /* width, height       */
                       10,                            /* border_width        */
                       XCB_WINDOW_CLASS_INPUT_OUTPUT, /* class               */
                       screen->root_visual,           /* visual              */
                       0, NULL );                     /* masks, not used yet */

    xcb_gcontext_t  black    = xcb_generate_id (connection);
    uint32_t        mask     = XCB_GC_FOREGROUND;
    uint32_t        value[]  = { screen->black_pixel };

    xcb_create_gc (connection, black, *window, mask, value);

    auto screenshot = take_screenshot(connection, *window);

    xcb_map_window(connection, my_window);

    std::chrono::time_point<std::chrono::system_clock> start, end;
    for (int i = 0; i < 300; i++) {
//    while (true) {
        start = std::chrono::system_clock::now();
        auto screenshot = take_screenshot(connection, *window);
        auto dir = process(screenshot);
        xcb_put_image(connection, XCB_IMAGE_FORMAT_Z_PIXMAP, my_window, black, size.first, size.second, 0, 0, 0, screen->root_depth,
                      screenshot.size(), screenshot.data()
        );
        switch (dir) {
            case Direction::LEFT:
                xcb_test_fake_input(connection, XCB_KEY_RELEASE, RIGHT_KEYCODE, XCB_CURRENT_TIME, *window, 0, 0, 0);
                xcb_test_fake_input(connection, XCB_KEY_PRESS, LEFT_KEYCODE, XCB_CURRENT_TIME, *window, 0, 0, 0);
                break;
            case Direction::RIGHT:
                xcb_test_fake_input(connection, XCB_KEY_RELEASE, LEFT_KEYCODE, XCB_CURRENT_TIME, *window, 0, 0, 0);
                xcb_test_fake_input(connection, XCB_KEY_PRESS, RIGHT_KEYCODE, XCB_CURRENT_TIME, *window, 0, 0, 0);
                break;
            case Direction::STAY:
                xcb_test_fake_input(connection, XCB_KEY_RELEASE, LEFT_KEYCODE, XCB_CURRENT_TIME, *window, 0, 0, 0);
                xcb_test_fake_input(connection, XCB_KEY_RELEASE, RIGHT_KEYCODE, XCB_CURRENT_TIME, *window, 0, 0, 0);
                break;
            case Direction::UNKNOWN:
                xcb_test_fake_input(connection, XCB_KEY_RELEASE, LEFT_KEYCODE, XCB_CURRENT_TIME, *window, 0, 0, 0);
                xcb_test_fake_input(connection, XCB_KEY_RELEASE, RIGHT_KEYCODE, XCB_CURRENT_TIME, *window, 0, 0, 0);
                xcb_test_fake_input(connection, XCB_KEY_PRESS, SPACE_KEYCODE, XCB_CURRENT_TIME, *window, 0, 0, 0);
                xcb_flush(connection);
                usleep(10000);
                xcb_test_fake_input(connection, XCB_KEY_RELEASE, SPACE_KEYCODE, XCB_CURRENT_TIME, *window, 0, 0, 0);
                xcb_flush(connection);
                break;

        }

        xcb_flush(connection);

        end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end-start;
        BOOST_LOG_TRIVIAL(debug) << "Time to fetch frame: " << elapsed_seconds.count();
    }


    return 0;

    while (1)
    {
        usleep(100000);
        xcb_test_fake_input(connection, XCB_KEY_PRESS, LEFT_KEYCODE, XCB_CURRENT_TIME, *window, 0, 0, 0);
        xcb_flush(connection);
        usleep(100000);
        xcb_test_fake_input(connection, XCB_KEY_RELEASE, LEFT_KEYCODE, XCB_CURRENT_TIME, *window, 0, 0, 0);
        xcb_flush(connection);
        BOOST_LOG_TRIVIAL(debug) << "lol";
    }

    return 0;
}    
