#ifndef HEXABOT_SCREENSHOT_H
#define HEXABOT_SCREENSHOT_H

#include <cstdint>
#include <cstddef>
#include <vector>

class Screenshot
{
    std::vector<uint8_t> data_;
public:
    const size_t width, height;

    Screenshot(std::vector<uint8_t>&&, size_t width, size_t height);
    void set(size_t x, size_t y, uint32_t val);
    uint32_t get(size_t x, size_t y) const;
    uint8_t* data();
    uint8_t const* data() const;
    size_t size() const;
};


#endif //HEXABOT_SCREENSHOT_H
