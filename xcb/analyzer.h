#ifndef HEXABOT_ANALYZER_H
#define HEXABOT_ANALYZER_H

#include "screenshot.h"

enum class Direction {
    LEFT, RIGHT, STAY, UNKNOWN
};

Direction process(Screenshot& screenshot);

#endif //HEXABOT_ANALYZER_H
