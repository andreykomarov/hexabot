#include <stdint.h>
#include <deque>
#include <utility>
#include <map>
#include <set>
#include <boost/log/trivial.hpp>
#include <stdlib.h>
#include <math.h>

#include "analyzer.h"

static int dist(uint32_t a, uint32_t b) {
    uint8_t *pa = (uint8_t*)&a;
    uint8_t *pb = (uint8_t*)&b;
    int sum = 0;
    for (int i = 0; i < 4; i++) {
        sum += abs(pa[i] - pb[i]) * abs(pa[i] - pb[i]);
    }
    return sqrt(sum);
}

Direction process(Screenshot& screenshot)
{
    size_t width = screenshot.width;
    size_t height = screenshot.height;
    const int MAX_RADIUS = std::min(width, height) / 2 - 2;
    std::vector<std::vector<size_t>> used(width, std::vector<size_t>(height));
    size_t x = width / 2;
    size_t y = height / 2;
    uint32_t center_color = screenshot.get(x, y);
    while (y > 0 && screenshot.get(x, y) == center_color) {
        screenshot.set(x, y, 0x00ffff00);
        y--;
    }
    if (y == 0) {
        BOOST_LOG_TRIVIAL(error) << "Obstacles not found";
        return Direction::UNKNOWN;
    }
    size_t fail_y = y;
    size_t cx = width / 2;
    size_t cy = height / 2;
    const size_t span = (height / 2 - fail_y);
    uint32_t obstacle_color = screenshot.get(x, y);
    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            if (dist(obstacle_color, screenshot.get(i, j)) < 80)
                screenshot.set(i, j, 0);
            else
                screenshot.set(i, j, 0x55555555);
        }
    }
    obstacle_color = 0;

    std::map<int, size_t> component_size;
    std::map<int, std::pair<int, int>> center;

    std::pair<size_t, size_t> pointer;
    size_t smallest = height * width;
    size_t smallest_tag = 0;

    auto bounds = [&](int x, int y) {
        return 0 <= x && x < width && 0 <= y && y < height;
    };

    auto points = [&](int x1, int y1, int x2, int y2) {
        if (!bounds(x1, y1) || !bounds(x2, y2)) {
            BOOST_LOG_TRIVIAL(debug) << x1 << " " << y1 << " " << x2 << " " << y2;
        }
        BOOST_ASSERT(bounds(x1, y1) && bounds(x2, y2));
        std::vector<std::pair<int, int>> res;
        int len = abs(x1 - x2) + abs(y1 - y2);
        for (int i = 0; i < len; i++) {
            int x = x1 + i * (x2 - x1) / len;
            int y = y1 + i * (y2 - y1) / len;
            res.push_back({x, y});
        }
        return res;
    };

    auto get_radius = [&](int x, int y, double angle) {
        int r = 0;
        while (r < MAX_RADIUS) {
            int x2 = x + r * cos(angle);
            int y2 = y + r * sin(angle);
            if (screenshot.get(x2, y2) == 0) {
                return r;
            }
            r++;
        }
        return r;
    };

    auto draw_line= [&](int x1, int y1, int x2, int y2, uint32_t color) {
        BOOST_ASSERT(bounds(x1, y1) && bounds(x2, y2));
        for (auto p : points(x1, y1, x2, y2)) {
            screenshot.set(p.first, p.second, color);
        }
    };
    auto draw_rect = [&](int x, int y, uint32_t color, int radius = 5) {
        for (int dx = -radius; dx < radius; dx++) {
            for (int dy = -radius; dy < radius; dy++) {
                if (bounds(x + dx, y + dy)) {
                    screenshot.set(x + dx, y + dy, color);
                }
            }
        }
    };

    auto bfs = [&](size_t startx, size_t starty) {
        if (used[startx][starty])
            return;
        size_t tag = component_size.size() + 1;
        uint32_t color = tag * 0x14881488;
        uint32_t expected_color = screenshot.get(startx, starty);
        used[startx][starty] = tag;
        std::deque<std::pair<int, int>> q;
        q.push_back({startx, starty});
        size_t count = 0;
        long long sum_x = 0;
        long long sum_y = 0;
        while (q.size()) {
            count++;
            auto p = q.front();
            sum_x += p.first;
            sum_y += p.second;
            q.pop_front();
//            screenshot.set(p.first, p.second, color);
            int d[4][2] = {{0, -1}, {0, 1}, {1, 0}, {-1, 0}};
            for (int i = 0; i < 4; i++) {
                int x = p.first + d[i][0];
                int y = p.second + d[i][1];
                if (!bounds(x, y))
                    continue;
                if (used[x][y] || screenshot.get(x, y) != expected_color)
                    continue;
                q.push_back({x, y});
                used[x][y] = tag;
            }
        }
        component_size[tag] = count;
        int x = sum_x / count;
        int y = sum_y / count;
        center[tag] = {x, y};
        double d = sqrt((x - cy) * (x - cx) + (y - cy) * (y - cy));
        if (count < smallest && d > span && count < 200) {
            smallest = count;
            BOOST_LOG_TRIVIAL(debug) << "update " << sum_x << " " << sum_y << " " << count;
            pointer = {x, y};
            smallest_tag = tag;
        }
    };
    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            double d = sqrt((i - cx) * (i - cx) + (j - cy) * (j - cy));
            if (span < d && d < 3 * span && dist(screenshot.get(i, j), obstacle_color) < 100)
                bfs(i, j);
        }
    }

    if (smallest_tag == 0) {
        BOOST_LOG_TRIVIAL(error) << "Pointer not found";
        return Direction::UNKNOWN;
    }

    double ptr_angle = atan2(pointer.second - (double)cy, pointer.first - (double)cx);
    BOOST_LOG_TRIVIAL(debug) << "angle " << ptr_angle << " ptr " << pointer.first << " " << pointer.second;
//    draw_line(cx, cy, cx + cos(ptr_angle) * 100, cy + sin(ptr_angle) * 100, 0xffffffff);
    size_t ptr_radius = sqrt((pointer.first - cx) * (pointer.first - cx) + (pointer.second - cy) * (pointer.second - cy));
    BOOST_LOG_TRIVIAL(debug) << "radius " << ptr_radius;
    if (ptr_radius > 110)
        return Direction::UNKNOWN;

    bfs(cx, cy);
    BOOST_LOG_TRIVIAL(debug) << "old center: " << cx << " " << cy;
    cx = center[used[cx][cy]].first;
    cy = center[used[cx][cy]].second;
    BOOST_LOG_TRIVIAL(debug) << "new center: " << cx << " " << cy;

    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            if (used[i][j] == smallest_tag) {
                screenshot.set(i, j, 0xffffffff);
            }
        }
    }

    const int rays_cnt = 60;
    struct {
        double angle;
        int radius;
    } rays[rays_cnt];
    int max_inner_radius = 0;
    for (int i = 0; i < rays_cnt; i++) {
        rays[i].angle = ptr_angle + i * 2 * M_PI / rays_cnt;
        rays[i].radius = get_radius(cx, cy, rays[i].angle);
        max_inner_radius = std::max(max_inner_radius, rays[i].radius);
    }
    double multiplier = height / 2 / max_inner_radius - 1;
    BOOST_LOG_TRIVIAL(debug) << "Multiplier " << multiplier;

    const int ON_RAY = 30;
    auto coords = [&](int i, int j) -> std::pair<int, int> {
        double angle = rays[i].angle;
        double r = rays[i].radius * (1 + multiplier * j / ON_RAY);
        double x = cx + r * cos(angle);
        double y = cy + r * sin(angle);
        if (abs(x) > 1000 || abs(y) > 1000) {
            BOOST_LOG_TRIVIAL(warning) << "Bad coords radius=" << rays[i].radius << " mult=" << multiplier << " pos=" << i << " " << j;
            throw std::exception();
        }
        return {x, y};
    };

    typedef std::pair<int, int> vertex;
    auto good = [&](const vertex& v1, const vertex& v2) {
        auto p1 = coords(v1.first, v1.second);
        auto p2 = coords(v2.first, v2.second);
//        {
//            int dx = p2.first - p1.first;
//            int dy = p2.second - p1.second;
//            p1.first -= dx / 2;
//            p2.first += dx / 2;
//            p1.second -= dy / 2;
//            p2.second += dy / 2;
//        }

        for (auto p : points(p1.first, p1.second, p2.first, p2.second)) {
            if (screenshot.get(p.first, p.second) == 0) {
                return false;
            }
        }
        return true;
    };

    for (int i = 0; i < rays_cnt; i++) {
        for (int j = 0; j < ON_RAY - 1; j++) {
            auto p1 = std::make_pair(i, j);
            auto p2 = std::make_pair((i + 1) % rays_cnt, j);
            if (good(p1, p2)) {
                auto c1 = coords(p1.first, p1.second);
                auto c2 = coords(p2.first, p2.second);
                draw_line(c1.first, c1.second, c2.first, c2.second, 0x00ffff00);
            }
            p2 = std::make_pair(i, j + 1);
            if (good(p1, p2)) {
                auto c1 = coords(p1.first, p1.second);
                auto c2 = coords(p2.first, p2.second);
                draw_line(c1.first, c1.second, c2.first, c2.second, 0x00ffff00);
            }
        }
    }

    std::map<vertex, vertex> prev;
    std::deque<vertex> q;
    vertex start = {0, 1};
    std::set<vertex> used2;
    used2.insert(start);
    q.push_back(start);
    vertex winner = start;

    while (q.size()) {
        vertex now = q.front();
        if (now.second == ON_RAY - 1) {
            winner = now;
            break;
        }
        q.pop_front();
        vertex edges[] = {
                {(now.first + 1) % rays_cnt, now.second},
                {(now.first + rays_cnt - 1) % rays_cnt, now.second},
                {now.first, now.second + 1}};
        for (auto to : edges) {
            if (!good(now, to))
                continue;
            if (used2.count(to))
                continue;
            used2.insert(to);
            prev[to] = now;
            q.push_back(to);
        }
    }
    if (winner == start) {
        return Direction::STAY;
    }
    std::vector<vertex> path;
    while (winner != start) {
        path.push_back(winner);
        auto pos = coords(winner.first, winner.second);
        draw_rect(pos.first, pos.second, 0x00ffffff);
        winner = prev[winner];
    }
    std::reverse(path.begin(), path.end());
    auto v = path[0];
    if (v.first == 0)
        v = path[1];
    if (v.first == 1) {
        draw_rect(100, 100, 0xffffffff, 50);
        return Direction::RIGHT;
    } else if (v.first == rays_cnt - 1){
        draw_rect(600, 100, 0xffffffff, 50);
        return Direction::LEFT;
    }

    return Direction::STAY;
}
