#include "screenshot.h"
#include <boost/log/trivial.hpp>

Screenshot::Screenshot(std::vector<uint8_t>&& data, size_t width, size_t height) :
        data_(data), width(width), height(height) {
}

uint32_t Screenshot::get(size_t x, size_t y) const {
    size_t off = width * y + x;
    uint32_t *pixels = (uint32_t*)data();
    return pixels[off];
}

void Screenshot::set(size_t x, size_t y, uint32_t val) {
    size_t off = width * y + x;
    uint32_t *pixels = (uint32_t*)data();
    pixels[off] = val;
}

uint8_t *Screenshot::data() {
    return data_.data();
}

uint8_t const* Screenshot::data() const {
    return data_.data();
}

size_t Screenshot::size() const {
    return data_.size();
}


