#include "hexagon.h"
#include "screenshot.h"

#include <boost/log/trivial.hpp>

const std::string HEXAGON = "Super Hexagon";

static boost::optional<xcb_window_t> search(xcb_connection_t *connection, xcb_window_t root) {
    {
        xcb_atom_t property = XCB_ATOM_WM_NAME;
        xcb_atom_t type = XCB_ATOM_STRING;
        xcb_get_property_cookie_t cookie = xcb_get_property(connection, 0, root, property, type, 0, 100);
        xcb_get_property_reply_t *reply;
        if ((reply = xcb_get_property_reply(connection, cookie, NULL))) {
            int len = xcb_get_property_value_length(reply);
            if (len > 0) {
                char *name = (char *) xcb_get_property_value(reply);
                if (name == HEXAGON) {
                    BOOST_LOG_TRIVIAL(debug) << "Found window with name \"" << name << "\" (id=" << root << ")";
                    free(reply);
                    return root;
                }
            }
        }
        free(reply);
    }

    xcb_query_tree_cookie_t cookie = xcb_query_tree(connection, root);
    xcb_query_tree_reply_t *reply;
    if ((reply = xcb_query_tree_reply(connection, cookie, NULL))) {
        xcb_window_t *children = xcb_query_tree_children(reply);
        for (int i = 0; i < xcb_query_tree_children_length(reply); i++)
            if (auto res = search(connection, children[i]))
                return res;
    }
    return boost::none;
}

boost::optional<xcb_window_t> get_hexagon_window(xcb_connection_t *connection) {
    BOOST_LOG_TRIVIAL(debug) << "Searching for \"Super Hexagon\" window...";
    const xcb_setup_t *setup = xcb_get_setup(connection);
    xcb_screen_t *screen = xcb_setup_roots_iterator(setup).data;
    return search(connection, screen->root);
}

std::pair<uint16_t, uint16_t> get_window_size(xcb_connection_t *connection, xcb_window_t window)
{
    xcb_get_geometry_cookie_t cookie = xcb_get_geometry(connection, window);
    xcb_get_geometry_reply_t *reply;
    if ((reply = xcb_get_geometry_reply(connection, cookie, NULL))) {
        auto res = std::make_pair(reply->width, reply->height);
        free(reply);
        return res;
    }
    BOOST_LOG_TRIVIAL(error) << "Error during geometry request for window " << window;
    throw std::exception();
}

Screenshot take_screenshot(xcb_connection_t *connection, xcb_window_t window)
{
    auto size = get_window_size(connection, window);
    xcb_get_image_cookie_t cookie = xcb_get_image(connection, XCB_IMAGE_FORMAT_Z_PIXMAP, window, 0, 0, size.first, size.second, ~0);
    xcb_get_image_reply_t *reply;
    xcb_generic_error_t *err;
    if ((reply = xcb_get_image_reply(connection, cookie, &err)))
    {
        int len = xcb_get_image_data_length(reply);
        uint8_t *data = xcb_get_image_data(reply);
        std::vector<uint8_t> res(data, data + len);
        free(reply);
        BOOST_LOG_TRIVIAL(debug) << "len = " << len;
        return Screenshot(std::move(res), size.first, size.second);
    }
    BOOST_LOG_TRIVIAL(error) << "Err " << (int)err->error_code;
    throw std::exception();
}