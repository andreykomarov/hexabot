#ifndef HEXABOT_HEXAGON_H
#define HEXABOT_HEXAGON_H

#include <xcb/xcb.h>
#include <boost/optional.hpp>
#include <utility>

#include "screenshot.h"

boost::optional<xcb_window_t> get_hexagon_window(xcb_connection_t*);
std::pair<uint16_t, uint16_t> get_window_size(xcb_connection_t*, xcb_window_t);
Screenshot take_screenshot(xcb_connection_t *connection, xcb_window_t window);

#endif //HEXABOT_HEXAGON_H
